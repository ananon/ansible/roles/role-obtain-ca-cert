# Obtain ca certification

With the use of this role you can obtain the bundle of your openshift cluster.
The bundle would come in base64 encoding format.

## Role Variables

| Variable                      | Required | Default | Choices    | Comments                                                            |
| ----------------------------- | -------- | ------- | ---------- | ------------------------------------------------------------------- |
| out_var_bundle                  | yes      | ca_bundle      | -          | The bundle would come in base64 encoding format                                                                   |
